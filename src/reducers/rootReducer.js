import SquareHelper from '../components/Square/SquareHelper';
import CheckersHelper from '../components/Checkers/CheckersHelper';
import * as constants from '../constants';

const initState = {
  currentPlayerA: true,
  stepNumber: 0,
  history: [{
    squares: CheckersHelper.getStartSquares(),
  }],
  selected: false,
  highlighted: CheckersHelper.getInitStateHighlighted(),
  currentJumpingPiece: null,
  pieceBecameKing: false,
  winner: null,
};

const updateCurrentStepSquares = (state, squares) => {
  const newHistory = state.history.slice(0, state.stepNumber);
  newHistory[state.stepNumber] = { squares };
  return newHistory;
};

const getCurrentStepSquares = state => state.history[state.stepNumber].squares;

const rootReducer = (state = initState, action) => {
  switch (action.type) {
    case constants.DESELECT_SQUARES:
      return {
        ...state,
        selected: initState.selected,
        highlighted: initState.highlighted,
        currentJumpingPiece: initState.currentJumpingPiece,
      };
    case constants.SELECT_SQUARE: {
      const highlighted = CheckersHelper.getHighlights(action.squareIndex, state.currentJumpingPiece, getCurrentStepSquares(state), state.currentPlayerA);

      return { ...state, selected: action.squareIndex, highlighted };
    }
    case constants.CHECK_WINNER: {
      const winner = CheckersHelper.calculateWinner(getCurrentStepSquares(state));
      return { ...state, winner };
    }
    case constants.ADD_STEP: {
      // history gets a copy of the last step
      const newHistory = state.history.concat({ squares: state.history[state.stepNumber].squares.slice() });
      return { ...state, history: newHistory, stepNumber: state.stepNumber + 1 };
    }
    case constants.CHANGE_PLAYER:
      // if a piece became a king this turn (in moveSquare)
      // the current player's turn is ended (jump or move)
      if (state.pieceBecameKing) return state;
      return { ...state, currentPlayerA: !state.currentPlayerA };
    case constants.MOVE_SQUARE: {
      const newSquares = getCurrentStepSquares(state);
      newSquares[action.fromSquare] = SquareHelper.getEmptySquare();
      newSquares[action.toSquare] = action.square;
      return { ...state, history: updateCurrentStepSquares(state, newSquares) };
    }
    case constants.REMOVE_SQUARE: {
      const newSquares = getCurrentStepSquares(state);
      newSquares[action.squareIndex] = SquareHelper.getEmptySquare();
      return { ...state, history: updateCurrentStepSquares(state, newSquares) };
    }
    case constants.SET_JUMPING_PIECE:
      return { ...state, currentJumpingPiece: action.squareIndex };
    case constants.SET_MUST_JUMP: {
      const {
        currentPlayerA,
        currentJumpingPiece,
      } = state;

      const newSquares = getCurrentStepSquares(state).map((square, squareIndex, originalSquares) => {
        if (currentJumpingPiece !== null) return SquareHelper.makeSquareNotMustJump(square);
        const squareIsCurrentPlayers = (currentPlayerA) ? SquareHelper.isSquarePlayerA(square) : SquareHelper.isSquarePlayerB(square);
        if (!squareIsCurrentPlayers) return SquareHelper.makeSquareNotMustJump(square);
        if (!CheckersHelper.isAnyJumpAvailable(squareIndex, currentJumpingPiece, originalSquares, currentPlayerA)) return SquareHelper.makeSquareNotMustJump(square);
        return SquareHelper.makeSquareMustJump(square);
      });
      return { ...state, history: updateCurrentStepSquares(state, newSquares) };
    }
    case constants.CHANGE_PLAYER_IF_NO_JUMPS_LEFT: {
      if (CheckersHelper.isHighlightsEmpty(state.highlighted)) {
        return {
          ...state,
          // DESELECT_SQUARES
          selected: initState.selected,
          highlighted: initState.highlighted,
          currentJumpingPiece: initState.currentJumpingPiece,
          // CHANGE_PLAYER
          currentPlayerA: (state.pieceBecameKing) ? state.currentPlayerA : !state.currentPlayerA,
        };
      }
      return state;
    }
    case constants.SELECT_FIRST_MUST_JUMP_SQUARE: {
      if (state.selected && state.currentJumpingPiece) return state;

      const squareIndex = CheckersHelper.getFirstMustJumpPieceIndex(getCurrentStepSquares(state));
      if (!squareIndex) return state;

      const highlighted = CheckersHelper.getHighlights(squareIndex, state.currentJumpingPiece, getCurrentStepSquares(state), state.currentPlayerA);

      return {
        ...state,
        // SELECT_SQUARE
        selected: squareIndex,
        highlighted,
      };
    }
    case constants.SET_PIECE_BECAME_KING:
      return { ...state, pieceBecameKing: action.pieceBecameKing };
    default:
      return state;
  }
};

export default rootReducer;
