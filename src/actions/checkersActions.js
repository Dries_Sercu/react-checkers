import * as constants from '../constants';

export const deselectSquares = {
  type: constants.DESELECT_SQUARES,
};

export const selectSquare = squareIndex => ({
  type: constants.SELECT_SQUARE,
  squareIndex,
});

export const checkWinner = {
  type: constants.CHECK_WINNER,
};

export const addStep = {
  type: constants.ADD_STEP,
};

export const changePlayer = {
  type: constants.CHANGE_PLAYER,
};

export const moveSquare = (fromSquare, toSquare, square) => ({
  type: constants.MOVE_SQUARE,
  fromSquare,
  toSquare,
  square,
});

export const setPieceBecameKing = pieceBecameKing => ({
  type: constants.SET_PIECE_BECAME_KING,
  pieceBecameKing,
});

export const removeSquare = squareIndex => ({
  type: constants.REMOVE_SQUARE,
  squareIndex,
});

export const setJumpingPiece = squareIndex => ({
  type: constants.SET_JUMPING_PIECE,
  squareIndex,
});

export const setMustJump = {
  type: constants.SET_MUST_JUMP,
};

export const changePlayerIfNoJumpsLeft = {
  type: constants.CHANGE_PLAYER_IF_NO_JUMPS_LEFT,
};

export const selectFirstMustJumpSquare = {
  type: constants.SELECT_FIRST_MUST_JUMP_SQUARE,
};
