import React from 'react';
import renderer from 'react-test-renderer';

import Checkers from '../Checkers';

describe('Checkers component', () => {
  it('Renders correctly', () => {
    const component = renderer.create(<Checkers />);
    expect(component.toJSON()).toMatchSnapshot();
  });
});
