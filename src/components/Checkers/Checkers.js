import React from 'react';
import { connect } from 'react-redux';
import propTypes from 'prop-types';

import { squareValueShape } from '../../types';
import * as checkersActions from '../../actions/checkersActions';

import SquareHelper from '../Square/SquareHelper';
import CheckersHelper from './CheckersHelper';
import Board from '../Board/Board';

import './checkers.scss';

class Checkers extends React.Component {
  getSquares() {
    const { history, stepNumber } = this.props;
    return history[stepNumber].squares;
  }

  movePiece(fromSquare, toSquare) {
    const {
      currentJumpingPiece,
      currentPlayerA,
      dispatchAddStep,
      dispatchChangePlayer,
      dispatchDeselectSquares,
      dispatchMoveSquare,
      dispatchSetPieceBecameKing,
      dispatchRemoveSquare,
      dispatchSelectSquare,
      dispatchSetJumpingPiece,
      dispatchSetMustJump,
      dispatchChangePlayerIfNoJumpsLeft,
      dispatchSelectFirstMustJumpSquare,
    } = this.props;
    const squares = this.getSquares();
    if (!CheckersHelper.isMoveLegal(fromSquare, toSquare, currentJumpingPiece, squares, currentPlayerA)) return;

    dispatchAddStep();
    dispatchChangePlayer();

    // upgrade piece if needed
    const movedSquare = CheckersHelper.makeSquareKing(toSquare, squares[fromSquare], currentPlayerA);
    const pieceBecameKing = SquareHelper.isSquareKing(movedSquare);

    dispatchDeselectSquares();
    dispatchMoveSquare(fromSquare, toSquare, movedSquare);
    if (pieceBecameKing) dispatchSetPieceBecameKing(true);

    const jumpTarget = CheckersHelper.getJumpTarget(fromSquare, toSquare);
    if (jumpTarget) {
      // when a jump is made, the jumped piece must be removed
      dispatchRemoveSquare(jumpTarget);
      // the player's turn shouldn't change after a jump (so switch back)
      dispatchChangePlayer();
      // set current jumping piece & selected
      dispatchSetJumpingPiece(toSquare);
      dispatchSelectSquare(toSquare);

      dispatchChangePlayerIfNoJumpsLeft(toSquare);
    }

    dispatchSetMustJump();
    dispatchSelectFirstMustJumpSquare();

    // reset PieceBecameKing
    if (pieceBecameKing) dispatchSetPieceBecameKing(false);
  }

  handleSquareClick(squareIndex) {
    const {
      currentJumpingPiece,
      currentPlayerA,
      selected,
      winner,
      dispatchSelectSquare,
      dispatchDeselectSquares,
      dispatchCheckWinner,
    } = this.props;

    if (winner) return;

    const squares = this.getSquares();

    if (selected && !CheckersHelper.isSquareOwnedByPlayer(squareIndex, squares, currentPlayerA)) {
      if (CheckersHelper.isSquareEmpty(squareIndex, squares)) {
        this.movePiece(selected, squareIndex);
        dispatchCheckWinner();
      }
      return;
    }

    if (CheckersHelper.isSquareEmpty(squareIndex, squares) || !CheckersHelper.isSquareOwnedByPlayer(squareIndex, squares, currentPlayerA)) {
      return;
    }

    // can't deselect when player is jumping
    if (currentJumpingPiece !== squareIndex && squareIndex === selected) {
      dispatchDeselectSquares();
      return;
    }

    // can't highlight other square when player is jumping
    if (currentJumpingPiece === selected) {
      return;
    }

    // if any square for the currentPlayer can jump, only those squares can be selected
    if (CheckersHelper.hasMustJumpSquares(squares) && !CheckersHelper.isSquareMustJump(squareIndex, squares)) {
      return;
    }

    dispatchSelectSquare(squareIndex);
  }

  render() {
    let boardClassname;
    const {
      currentPlayerA,
      selected,
      highlighted,
      winner,
    } = this.props;
    if (!winner) {
      boardClassname = (currentPlayerA) ? 'player-A' : 'player-B';
    }
    const squares = this.getSquares();
    return (
      <div>
        <Board
          className={boardClassname}
          squares={squares}
          onClick={i => this.handleSquareClick(i)}
          selected={selected}
          highlighted={highlighted}
        />
      </div>
    );
  }
}

Checkers.propTypes = {
  currentJumpingPiece: propTypes.number,
  currentPlayerA: propTypes.bool.isRequired,
  highlighted: propTypes.arrayOf(propTypes.bool).isRequired,
  history: propTypes.arrayOf(propTypes.shape({
    squares: propTypes.arrayOf(squareValueShape),
  })).isRequired,
  pieceBecameKing: propTypes.bool,
  selected: propTypes.oneOfType([
    propTypes.number,
    propTypes.bool, // false
  ]).isRequired,
  stepNumber: propTypes.number.isRequired,
  winner: propTypes.oneOf(['A', 'B', null]),

  dispatchSelectSquare: propTypes.func.isRequired,
  dispatchDeselectSquares: propTypes.func.isRequired,
  dispatchCheckWinner: propTypes.func.isRequired,
  dispatchAddStep: propTypes.func.isRequired,
  dispatchChangePlayer: propTypes.func.isRequired,
  dispatchMoveSquare: propTypes.func.isRequired,
  dispatchSetPieceBecameKing: propTypes.func.isRequired,
  dispatchRemoveSquare: propTypes.func.isRequired,
  dispatchSetJumpingPiece: propTypes.func.isRequired,
  dispatchSetMustJump: propTypes.func.isRequired,
  dispatchChangePlayerIfNoJumpsLeft: propTypes.func.isRequired,
  dispatchSelectFirstMustJumpSquare: propTypes.func.isRequired,
};

// THIS IS SET IN THE rootReducer
Checkers.defaultProps = {
  currentJumpingPiece: null,
  pieceBecameKing: false,
  winner: null,
};

const mapStateToProps = state => ({
  currentJumpingPiece: state.currentJumpingPiece,
  currentPlayerA: state.currentPlayerA,
  highlighted: state.highlighted,
  history: state.history,
  pieceBecameKing: state.pieceBecameKing,
  selected: state.selected,
  stepNumber: state.stepNumber,
  winner: state.winner,
});

const mapDispatchToProps = dispatch => ({
  dispatchSelectSquare: squareIndex => dispatch(checkersActions.selectSquare(squareIndex)),
  dispatchDeselectSquares: () => dispatch(checkersActions.deselectSquares),
  dispatchCheckWinner: () => dispatch(checkersActions.checkWinner),
  dispatchAddStep: () => dispatch(checkersActions.addStep),
  dispatchChangePlayer: () => dispatch(checkersActions.changePlayer),
  dispatchMoveSquare: (fromSquare, toSquare, square) => dispatch(checkersActions.moveSquare(fromSquare, toSquare, square)),
  dispatchSetPieceBecameKing: piecebecameKing => dispatch(checkersActions.setPieceBecameKing(piecebecameKing)),
  dispatchRemoveSquare: squareIndex => dispatch(checkersActions.removeSquare(squareIndex)),
  dispatchSetJumpingPiece: squareIndex => dispatch(checkersActions.setJumpingPiece(squareIndex)),
  dispatchSetMustJump: () => dispatch(checkersActions.setMustJump),
  dispatchChangePlayerIfNoJumpsLeft: () => dispatch(checkersActions.changePlayerIfNoJumpsLeft),
  dispatchSelectFirstMustJumpSquare: () => dispatch(checkersActions.selectFirstMustJumpSquare),
});

export default connect(mapStateToProps, mapDispatchToProps)(Checkers);
