import SquareHelper from '../Square/SquareHelper';

export default class CheckersHelper {
  static getStartSquares() {
    return [
      SquareHelper.getEmptySquare(),
      SquareHelper.getPlayerASquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getPlayerASquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getPlayerASquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getPlayerASquare(),
      SquareHelper.getPlayerASquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getPlayerASquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getPlayerASquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getPlayerASquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getPlayerASquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getPlayerASquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getPlayerASquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getPlayerASquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getPlayerBSquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getPlayerBSquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getPlayerBSquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getPlayerBSquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getPlayerBSquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getPlayerBSquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getPlayerBSquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getPlayerBSquare(),
      SquareHelper.getPlayerBSquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getPlayerBSquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getPlayerBSquare(),
      SquareHelper.getEmptySquare(),
      SquareHelper.getPlayerBSquare(),
      SquareHelper.getEmptySquare(),
    ];
  }

  static getInitStateHighlighted() {
    return Array(64).fill(false);
  }

  static getSquareRow(squareIndex) {
    return Math.floor(squareIndex / 8);
  }

  static getRowDistanceForSquares(squareOneIndex, squareTwoIndex) {
    if (squareOneIndex > squareTwoIndex) {
      return Math.abs(CheckersHelper.getSquareRow(squareOneIndex) - CheckersHelper.getSquareRow(squareTwoIndex));
    }
    return Math.abs(CheckersHelper.getSquareRow(squareTwoIndex) - CheckersHelper.getSquareRow(squareOneIndex));
  }

  static isHighlightsEmpty(highlights) {
    return !highlights.some(highlighted => highlighted);
  }

  static getJumpTarget(fromSquare, toSquare) {
    if (CheckersHelper.getRowDistanceForSquares(fromSquare, toSquare) === 2) {
      if (fromSquare > toSquare) {
        return fromSquare - ((fromSquare - toSquare) / 2);
      }
      return fromSquare + ((toSquare - fromSquare) / 2);
    }
    return false;
  }

  static getSquare(squareIndex, squares) {
    if (!CheckersHelper.isSquareInGame(squareIndex)) return null;
    return squares[squareIndex];
  }

  static getHighlights(squareIndex, currentJumpingPiece, squares, playerA) {
    const highlighted = CheckersHelper.getInitStateHighlighted();
    if (!squareIndex) return highlighted;

    const jumpTopLeft = squareIndex - 18;
    const jumpTopRight = squareIndex - 14;
    const jumpBottomLeft = squareIndex + 14;
    const jumpBottomRight = squareIndex + 18;

    if (CheckersHelper.isMoveLegal(squareIndex, jumpTopLeft, currentJumpingPiece, squares, playerA)) {
      highlighted[jumpTopLeft] = true;
    }
    if (CheckersHelper.isMoveLegal(squareIndex, jumpTopRight, currentJumpingPiece, squares, playerA)) {
      highlighted[jumpTopRight] = true;
    }
    if (CheckersHelper.isMoveLegal(squareIndex, jumpBottomLeft, currentJumpingPiece, squares, playerA)) {
      highlighted[jumpBottomLeft] = true;
    }
    if (CheckersHelper.isMoveLegal(squareIndex, jumpBottomRight, currentJumpingPiece, squares, playerA)) {
      highlighted[jumpBottomRight] = true;
    }

    // if player is jumping, don't highlight normal moves
    // if one option to jump is available, don't highlight normal moves
    if (currentJumpingPiece !== null || !CheckersHelper.isHighlightsEmpty(highlighted)) {
      return highlighted;
    }

    const topLeft = squareIndex - 9;
    const topRight = squareIndex - 7;
    const bottomLeft = squareIndex + 7;
    const bottomRight = squareIndex + 9;

    if (CheckersHelper.isMoveLegal(squareIndex, topLeft, currentJumpingPiece, squares, playerA)) {
      highlighted[topLeft] = true;
    }
    if (CheckersHelper.isMoveLegal(squareIndex, topRight, currentJumpingPiece, squares, playerA)) {
      highlighted[topRight] = true;
    }
    if (CheckersHelper.isMoveLegal(squareIndex, bottomLeft, currentJumpingPiece, squares, playerA)) {
      highlighted[bottomLeft] = true;
    }
    if (CheckersHelper.isMoveLegal(squareIndex, bottomRight, currentJumpingPiece, squares, playerA)) {
      highlighted[bottomRight] = true;
    }
    return highlighted;
  }

  static hasMustJumpSquares(squares) {
    return squares.some(square => SquareHelper.isSquareMustJump(square));
  }

  static isSquareMustJump(squareIndex, squares) {
    const square = CheckersHelper.getSquare(squareIndex, squares);
    return SquareHelper.isSquareMustJump(square);
  }

  // after one jump a normal piece can jump back in the "homeward" direction like a king
  static canMoveUp(squareIndex, currentJumpingPiece, squares, currentPlayerA) {
    if (currentPlayerA) {
      return SquareHelper.isSquareKing(CheckersHelper.getSquare(squareIndex, squares)) || currentJumpingPiece === squareIndex;
    }
    return true;
  }

  static canMoveDown(squareIndex, currentJumpingPiece, squares, currentPlayerA) {
    if (currentPlayerA) {
      return true;
    }
    return SquareHelper.isSquareKing(CheckersHelper.getSquare(squareIndex, squares)) || currentJumpingPiece === squareIndex;
  }

  static isSquareOwnedByPlayer(squareIndex, squares, playerA) {
    if (!CheckersHelper.isSquareInGame(squareIndex)) return false;
    const targetSquare = CheckersHelper.getSquare(squareIndex, squares);
    if (playerA) {
      return SquareHelper.isSquarePlayerA(targetSquare);
    }
    return SquareHelper.isSquarePlayerB(targetSquare);
  }

  // checks if the fromSquare can move to the toSquare
  static isMoveJump(fromSquare, toSquare, squareDiff, squares, playerA) {
    if ((fromSquare + squareDiff) !== toSquare) return false;
    // is toSquare an empty square
    if (!CheckersHelper.isSquareEmpty(toSquare, squares)) return false;

    const jumpTargetIndex = fromSquare + (squareDiff / 2);
    // is the square being jumped owned by opposing player (!empty & !currentplayer)
    if (CheckersHelper.isSquareEmpty(jumpTargetIndex, squares)) return false;

    if (CheckersHelper.isSquareOwnedByPlayer(jumpTargetIndex, squares, playerA)) return false;

    return true;
  }

  static isSquareInGame(squareIndex) {
    return squareIndex > 0 && squareIndex < 63;
  }

  static isSquareEmpty(squareIndex, squares) {
    if (!CheckersHelper.isSquareInGame(squareIndex)) return false;
    const targetSquare = CheckersHelper.getSquare(squareIndex, squares);
    return SquareHelper.isSquareEmpty(targetSquare);
  }

  static isJumpAvailable(fromSquare, toSquare, currentJumpingPiece, squares, playerA) {
    // check if specific jump is available
    if (CheckersHelper.canMoveUp(fromSquare, currentJumpingPiece, squares, playerA)) {
      if (CheckersHelper.isMoveJump(fromSquare, toSquare, -14, squares, playerA)
        || CheckersHelper.isMoveJump(fromSquare, toSquare, -18, squares, playerA)) return true;
    }
    if (CheckersHelper.canMoveDown(fromSquare, currentJumpingPiece, squares, playerA)) {
      if (CheckersHelper.isMoveJump(fromSquare, toSquare, 14, squares, playerA)
        || CheckersHelper.isMoveJump(fromSquare, toSquare, 18, squares, playerA)) return true;
    }
    return false;
  }

  static isAnyJumpAvailable(fromSquare, currentJumpingPiece, squares, playerA) {
    let jumpAvailable = false;
    let toSquare;

    if (CheckersHelper.canMoveUp(fromSquare, currentJumpingPiece, squares, playerA)) {
      toSquare = fromSquare - 14;
      if (!jumpAvailable && CheckersHelper.getRowDistanceForSquares(fromSquare, toSquare) === 2) {
        jumpAvailable = CheckersHelper.isMoveJump(fromSquare, toSquare, -14, squares, playerA);
      }
      toSquare = fromSquare - 18;
      if (!jumpAvailable && CheckersHelper.getRowDistanceForSquares(fromSquare, toSquare) === 2) {
        jumpAvailable = CheckersHelper.isMoveJump(fromSquare, toSquare, -18, squares, playerA);
      }
    }
    if (CheckersHelper.canMoveDown(fromSquare, currentJumpingPiece, squares, playerA)) {
      toSquare = fromSquare + 14;
      if (!jumpAvailable && CheckersHelper.getRowDistanceForSquares(fromSquare, toSquare) === 2) {
        jumpAvailable = CheckersHelper.isMoveJump(fromSquare, toSquare, 14, squares, playerA);
      }
      toSquare = fromSquare + 18;
      if (!jumpAvailable && CheckersHelper.getRowDistanceForSquares(fromSquare, toSquare) === 2) {
        jumpAvailable = CheckersHelper.isMoveJump(fromSquare, toSquare, 18, squares, playerA);
      }
    }

    return jumpAvailable;
  }

  static isMoveLegal(fromSquare, toSquare, currentJumpingPiece, squares, playerA) {
    if (!CheckersHelper.isSquareInGame(toSquare)) return false;
    if (!CheckersHelper.isSquareEmpty(toSquare, squares)) return false;

    if (currentJumpingPiece === null && CheckersHelper.getRowDistanceForSquares(fromSquare, toSquare) === 1 && !CheckersHelper.isAnyJumpAvailable(fromSquare, currentJumpingPiece, squares, playerA)) {
    // check if piece can move in the desired direction
      if (fromSquare > toSquare && !CheckersHelper.canMoveUp(fromSquare, currentJumpingPiece, squares, playerA)) return false;
      if (fromSquare < toSquare && !CheckersHelper.canMoveDown(fromSquare, currentJumpingPiece, squares, playerA)) return false;

      // check if the move is diagonal
      return (fromSquare - 7 === toSquare
        || fromSquare - 9 === toSquare
        || fromSquare + 7 === toSquare
        || fromSquare + 9 === toSquare);
    }
    if (CheckersHelper.getRowDistanceForSquares(fromSquare, toSquare) === 2) {
      return CheckersHelper.isJumpAvailable(fromSquare, toSquare, currentJumpingPiece, squares, playerA);
    }
    return false;
  }

  static isSquareInLastRow(squareIndex, currentPlayerA) {
    if (currentPlayerA) return CheckersHelper.getSquareRow(squareIndex) === 7;
    return CheckersHelper.getSquareRow(squareIndex) === 0;
  }

  static makeSquareKing(squareIndex, square, currentPlayerA) {
    if (CheckersHelper.isSquareInLastRow(squareIndex, currentPlayerA)) return SquareHelper.makeSquareKing(square);
    return square;
  }

  static calculateWinner(squares) {
    if (!squares.some(square => SquareHelper.isSquarePlayerA(square))) {
      return 'B';
    }
    if (!squares.some(square => SquareHelper.isSquarePlayerB(square))) {
      return 'A';
    }
    return null;
  }

  static getFirstMustJumpPieceIndex(squares) {
    const mustJumpSquares = squares.filter(square => square.mustjump);

    if (mustJumpSquares.length === 1) {
      return squares.findIndex(square => square.mustjump);
    }
    return null;
  }
}
