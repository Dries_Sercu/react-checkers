import React, { Component } from 'react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';

class CheckersStatus extends Component {
  getStatusClassName() {
    const { currentPlayerA, winner } = this.props;
    if (winner) return '';
    const currentPlayer = (currentPlayerA) ? 'A' : 'B';
    return `player-${currentPlayer}`;
  }

  getStatus() {
    const { currentPlayerA, winner } = this.props;
    if (winner) {
      return `Congratulations Player ${winner}!`;
    }
    const currentPlayer = (currentPlayerA) ? 'A' : 'B';
    return `Player ${currentPlayer}`;
  }

  render() {
    const statusClassname = this.getStatusClassName();
    const status = this.getStatus();
    return <h1 className={statusClassname}>{status}</h1>;
  }
}

CheckersStatus.propTypes = {
  currentPlayerA: propTypes.bool.isRequired,
  winner: propTypes.oneOf(['A', 'B', null]),
};

CheckersStatus.defaultProps = {
  winner: null,
};

const mapStateToProps = (state) => {
  const { currentPlayerA, winner } = state;
  return {
    currentPlayerA,
    winner,
  };
};

export default connect(mapStateToProps)(CheckersStatus);
