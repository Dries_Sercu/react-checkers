import React from 'react';
import propTypes from 'prop-types';

import { squareValueShape } from '../../types';
import SquareHelper from './SquareHelper';
import './square.scss';

export default function Square(props) {
  const {
    value, dark, highlight, selected, onClick,
  } = props;

  let squareClassName = 'square ';

  if (value.player) {
    squareClassName += `player-${value.player} `;
  }

  if (dark) {
    squareClassName += 'dark ';
  }
  if (highlight) {
    squareClassName += 'highlight ';
  }
  if (value.mustjump) {
    squareClassName += 'mustjump ';
  }
  if (selected) {
    squareClassName += 'selected ';
  }

  const pieceClassname = 'piece';
  const king = (value.king) ? '♔' : '';

  return (
    <button
      type="button"
      className={squareClassName.trim()}
      onClick={onClick}
    >
      <span className={pieceClassname}>{ king }</span>
    </button>
  );
}

Square.propTypes = {
  value: squareValueShape,
  dark: propTypes.bool,
  highlight: propTypes.bool,
  selected: propTypes.bool,
  onClick: propTypes.func,
};

Square.defaultProps = {
  value: SquareHelper.getEmptySquare(),
  dark: false,
  highlight: false,
  selected: false,
  onClick: () => {},
};
