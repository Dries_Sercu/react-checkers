import SquareHelper from '../SquareHelper';

describe('SquareHelper', () => {
  it('should get empty square', () => {
    const square = SquareHelper.getEmptySquare();

    const expectedSquare = {
      player: null,
      king: false,
      mustjump: false,
    };

    expect(square).toMatchObject(expectedSquare);
  });

  it('should get square for player A', () => {
    const square = SquareHelper.getPlayerASquare();

    const expectedSquare = {
      player: 'A',
      king: false,
      mustjump: false,
    };

    expect(square).toMatchObject(expectedSquare);
  });

  it('should get square for player B', () => {
    const square = SquareHelper.getPlayerBSquare();

    const expectedSquare = {
      player: 'B',
      king: false,
      mustjump: false,
    };

    expect(square).toMatchObject(expectedSquare);
  });

  it('should view square as empty', () => {
    const square = SquareHelper.getEmptySquare();
    expect(SquareHelper.isSquareEmpty(square)).toBeTruthy();
  });

  it('should not view player A square as empty', () => {
    const square = SquareHelper.getPlayerASquare();
    expect(SquareHelper.isSquareEmpty(square)).toBeFalsy();
  });

  it('should get player A square with mustjump and king', () => {
    const square = SquareHelper.getPlayerASquare(true, true);

    const expectedSquare = {
      player: 'A',
      king: true,
      mustjump: true,
    };

    expect(square).toMatchObject(expectedSquare);
  });

  it('should check square as player A\'s', () => {
    const square = SquareHelper.getPlayerASquare();
    expect(SquareHelper.isSquarePlayerA(square)).toBeTruthy();
  });

  it('should check square as not player B\'s', () => {
    const square = SquareHelper.getPlayerASquare();
    expect(SquareHelper.isSquarePlayerB(square)).toBeFalsy();
  });

  it('should make square king', () => {
    let square = SquareHelper.getPlayerASquare();
    square = SquareHelper.makeSquareKing(square);

    expect(square.king).toBeTruthy();
  });

  it('should detect square as king', () => {
    let square = SquareHelper.getPlayerASquare();
    square = SquareHelper.makeSquareKing(square);

    expect(SquareHelper.isSquareKing(square)).toBeTruthy();
  });

  it('should set mustjump on square to true', () => {
    let square = SquareHelper.getPlayerASquare();
    square = SquareHelper.makeSquareMustJump(square);

    expect(square.mustjump).toBeTruthy();
  });

  it('should set mustjump on square to true false', () => {
    let square = SquareHelper.getPlayerASquare(false, true);
    square = SquareHelper.makeSquareNotMustJump(square);

    expect(square.mustjump).toBeFalsy();
  });

  it('should detect square as mustjump', () => {
    let square = SquareHelper.getPlayerASquare(false, true);

    expect(SquareHelper.isSquareMustJump(square)).toBeTruthy();
  });
});
