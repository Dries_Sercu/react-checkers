import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import renderer from 'react-test-renderer';

import Square from '../Square';
import SquareHelper from '../SquareHelper';

describe('Square component', () => {
  it('should render square', () => {
    const component = renderer.create(<Square />);

    expect(component.toJSON()).toMatchSnapshot();
  });

  it('should add dark classname', () => {
    const component = mount(<Square dark />);

    expect(component.find('.square').hasClass('dark')).toBeTruthy();
  });

  it('should add highlight classname', () => {
    const component = mount(<Square highlight />);

    expect(component.find('.square').hasClass('highlight')).toBeTruthy();
  });

  it('should add selected classname', () => {
    const component = mount(<Square selected />);

    expect(component.find('.square').hasClass('selected')).toBeTruthy();
  });

  it('should add mustjump classname', () => {
    let square = SquareHelper.getPlayerASquare();
    square = SquareHelper.makeSquareMustJump(square);

    const component = mount(<Square value={square} />);

    expect(component.find('.square').hasClass('player-A')).toBeTruthy();
    expect(component.find('.square').hasClass('mustjump')).toBeTruthy();
  });

  it('should add all classnames', () => {
    let square = SquareHelper.getPlayerASquare();
    square = SquareHelper.makeSquareMustJump(square);

    const component = mount(<Square dark highlight selected value={square} />);

    expect(component.find('.square').hasClass('dark')).toBeTruthy();
    expect(component.find('.square').hasClass('highlight')).toBeTruthy();
    expect(component.find('.square').hasClass('selected')).toBeTruthy();
    expect(component.find('.square').hasClass('player-A')).toBeTruthy();
    expect(component.find('.square').hasClass('mustjump')).toBeTruthy();
  });

  it('should render king symbol', () => {
    let square = SquareHelper.getPlayerASquare();
    square = SquareHelper.makeSquareKing(square);

    const component = mount(<Square value={square} />);

    expect(toJson(component)).toMatchSnapshot();
  });

  it('should trigger onClick function when clicked', () => {
    const callback = jest.fn();
    const component = mount(<Square onClick={callback} />);

    component.find('.square').simulate('click');

    expect(callback.mock.calls.length).toEqual(1);
  });
});
