export default class SquareHelper {
  static getSquare(player = null, king = false, mustjump = false) {
    return {
      player,
      king,
      mustjump,
    };
  }

  static getEmptySquare() {
    return this.getSquare();
  }

  static isSquareEmpty(square) {
    return square.player === null;
  }

  static getPlayerASquare(king, mustjump) {
    return this.getPlayerSquare('A', king, mustjump);
  }

  static getPlayerBSquare(king, mustjump) {
    return this.getPlayerSquare('B', king, mustjump);
  }

  static getPlayerSquare(player, king, mustjump) {
    return this.getSquare(player, king, mustjump);
  }

  static isSquarePlayerA(square) {
    return square.player === 'A';
  }

  static isSquarePlayerB(square) {
    return square.player === 'B';
  }

  static makeSquareKing(square) {
    return Object.assign(square, { king: true });
  }

  static isSquareKing(square) {
    return square.king;
  }

  static makeSquareMustJump(square) {
    return Object.assign(square, { mustjump: true });
  }

  static makeSquareNotMustJump(square) {
    return Object.assign(square, { mustjump: false });
  }

  static isSquareMustJump(square) {
    return square.mustjump;
  }
}
