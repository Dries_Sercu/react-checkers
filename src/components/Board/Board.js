import React from 'react';
import propTypes from 'prop-types';

import { squareValueShape } from '../../types';
import Square from '../Square/Square';
import './board.scss';

export default class Board extends React.Component {
  renderSquare(col, row) {
    const squareId = (8 * (row - 1)) + col - 1;
    const {
      highlighted, selected, squares, onClick,
    } = this.props;
    const highlight = highlighted[squareId];
    const dark = (row % 2) ? Boolean(squareId % 2) : !(squareId % 2);

    return (
      <Square
        key={squareId}
        highlight={highlight}
        selected={selected === squareId}
        dark={dark}
        value={squares[squareId]}
        onClick={() => onClick(squareId)}
      />
    );
  }

  render() {
    const { className } = this.props;
    return (
      <div className={className}>
        {[1, 2, 3, 4, 5, 6, 7, 8].map(row => (
          <div key={row} className="board-row">
            {[1, 2, 3, 4, 5, 6, 7, 8].map(col => (this.renderSquare(col, row)))}
          </div>
        ))}
      </div>
    );
  }
}

Board.propTypes = {
  className: propTypes.string,
  highlighted: propTypes.arrayOf(propTypes.bool).isRequired,
  selected: propTypes.oneOfType([
    propTypes.number,
    propTypes.bool,
  ]),
  squares: propTypes.arrayOf(squareValueShape).isRequired,
  onClick: propTypes.func.isRequired,
};

Board.defaultProps = {
  className: '',
  selected: false,
};
