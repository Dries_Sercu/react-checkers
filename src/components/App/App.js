import React from 'react';

import CheckersStatus from '../CheckersStatus/CheckersStatus';
import Checkers from '../Checkers/Checkers';

const App = () => (
  <div>
    <CheckersStatus />
    <Checkers />
  </div>
);

export default App;
