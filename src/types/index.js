import propTypes from 'prop-types';

export const squareValueShape = propTypes.shape({
  player: propTypes.oneOf([null, 'A', 'B']),
  king: propTypes.bool,
  mustjump: propTypes.bool,
});
